class Business {
  String id;
  String name;
  String description;
  String websiteUrl;
  String coverPhoto;
  List<dynamic> pictures;
  String category;
  Location location;
  int likeCount;

  Business.fromJson(Map<String, dynamic> json)
      : id = json["_id"] ??= "",
        name = json["name"] ??= "",
        description = json["description"] ??= "",
        websiteUrl = json["websiteUrl"] ??= "",
        coverPhoto = json["coverPhoto"] ??= "",
        pictures = json["pictures"] ??= [],
        category = json["category"] ??= "",
        likeCount = json["likeCount"] ??= 0,
        location = Location.fromJson(json["location"]);
}

class Location {
  String type;
  List<dynamic> coordinates;
  Location.fromJson(dynamic json)
      : type = json["type"] ??= "",
        coordinates = json["coordinates"] ??= [];
}
