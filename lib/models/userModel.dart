class UserToSignup {
  String email;
  String name;
  String password;

  UserToSignup({
    required this.email,
    required this.name,
    required this.password,
  });

  Map<String, dynamic> toJson() => {
        'username': name,
        'email': email,
        'password': password,
      };
}

class UserToLogin {
  String username;
  String password;

  UserToLogin({
    required this.username,
    required this.password,
  });
  Map<String, dynamic> toJson() => {
        'username': username,
        'password': password,
      };
}

class User {
  String id;
  String email;
  String userName;
  List<String> favorites;

  User({
    required this.id,
    required this.email,
    required this.userName,
    required this.favorites,
  });
  Map<String, dynamic> toJson() => {
        '_id': id,
        'email': email,
        'username': userName,
        'favorites': favorites,
      };
  User.fromJson(dynamic json)
      : id = json["_id"] ??= "",
        userName = json["username"] ??= "",
        email = json["email"] ??= "",
        favorites = json["favorites"] ??= [];
}
