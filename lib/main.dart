import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:kirb_mobile/Screens/entrance/authenticationScreen.dart';
import 'package:kirb_mobile/Screens/mainRoutingScreen.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/services/data/businessServices.dart';
import 'package:kirb_mobile/services/handler/sharedPrefrenceHandler.dart';
import 'package:kirb_mobile/services/localization/myLocalization.dart';
import 'package:provider/provider.dart';

String? _jwt;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefrenceHandler.init();
  _jwt = SharedPrefrenceHandler.get("token");
  print(_jwt);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState? state = context.findAncestorStateOfType<_MyAppState>();
    print("and");
    state!.setLocale(locale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale? _locale;
  void setLocale(Locale locale) {
    setState(() {
      print(locale.languageCode);
      this._locale = locale;
      print("here");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => BusinessService(),
        ),
      ],
      child: MaterialApp(
        theme: customThemeData,
        home: _jwt == null ? AuthenticationScreen() : MainRoutingScreen(),
        locale: _locale,
        supportedLocales: [
          const Locale('am', 'ET'),
          const Locale('en', 'US'), // English, no country code
        ],
        localizationsDelegates: [
          MyLocalization.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == deviceLocale!.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return locale;
            }
          }
          return supportedLocales.first;
        },
      ),
    );
  }
}
