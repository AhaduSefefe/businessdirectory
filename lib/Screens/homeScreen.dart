import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/featuredList.dart';
import 'package:kirb_mobile/widgets/tagsList.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: size.height,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TagsList(),
              FeaturedList(),
            ],
          ),
        ),
      ),
    );
  }
}
