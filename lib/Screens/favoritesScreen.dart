import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/business/businessScreen.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/listItemContainer.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          children: [
            MyText(
              "favorites",
              style: bigText,
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: 10,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      // onTap: () => Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //       builder: (context) => BusinessScreen()),
                      // ),
                      child: ListItemContainer(
                        title: 'SafeWay SuperMarket',
                        subtitle: 'Detail Goes Here',
                        prefix: CustomIcons.arrowright_circle,
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
