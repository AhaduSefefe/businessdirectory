import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class BusinessMainScreen extends StatefulWidget {
  BusinessMainScreen({Key? key}) : super(key: key);

  @override
  _BusinessMainScreenState createState() => _BusinessMainScreenState();
}

class _BusinessMainScreenState extends State<BusinessMainScreen> {
  TextEditingController comment = TextEditingController();
  bool isCommentsExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 30, left: 30, right: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 50,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                boxShadow: inputShadow,
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  CustomIcons.chat,
                  size: 25,
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: TextField(
                  controller: comment,
                  textAlignVertical: TextAlignVertical.bottom,
                  decoration: InputDecoration(
                      border: InputBorder.none, hintText: "write your comment"),
                )),
                SizedBox(
                  width: 15,
                ),
                Icon(
                  CustomIcons.send,
                  size: 25,
                ),
              ],
            ),
          ),
          SizedBox(height: 30),
          Row(children: [Icon(CustomIcons.heart), Text('12')]),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                "comments",
                style: titleText,
              ),
              MyText(
                "5 comments",
                style: discriptionText,
              )
            ],
          ),
          Center(
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white)),
              child: MyText(
                isCommentsExpanded ? "close" : "view all",
                style: discriptionText,
              ),
              onPressed: () {
                setState(() {
                  this.isCommentsExpanded = !this.isCommentsExpanded;
                });
              },
            ),
          ),
          if (this.isCommentsExpanded) ...comments() else ...discription(),
        ],
      ),
    );
  }

  List<Widget> discription() {
    return [
      SizedBox(height: 15),
      MyText(
        "discription",
        style: titleText,
      ),
      SizedBox(height: 15),
      Text(
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi",
        style: discriptionText,
        textAlign: TextAlign.justify,
      ),
      SizedBox(
        height: 30,
      ),
      MyText(
        "pictures",
        style: titleText,
      ),
      Expanded(
        child: GridView.count(
          crossAxisSpacing: 15,
          mainAxisSpacing: 20,
          crossAxisCount: 2,
          children: List.generate(10, (index) {
            return Container(
              width: 150,
              height: 159,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(20)),
              clipBehavior: Clip.hardEdge,
              child: Image.asset(
                'assets/images/trialImage.jpg',
                fit: BoxFit.cover,
              ),
            );
          }),
        ),
      )
    ];
  }

  List<Widget> comments() {
    return [
      Expanded(
        child: ListView.builder(
          shrinkWrap: true,
          physics: FixedExtentScrollPhysics(),
          itemBuilder: (context, index) {
            return singleComment(
                name: "Amanuel Tamiru", comment: "Here is My Comment");
          },
          itemCount: 5,
        ),
      )
    ];
  }

  Widget singleComment({required String name, required String comment}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Text(name), Text(comment)],
      ),
    );
  }
}
