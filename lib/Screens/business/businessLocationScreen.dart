import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/models/businessModel.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class BusinessLocationScreen extends StatelessWidget {
  LatLng? latLng;
  BusinessLocationScreen({required Location location}) {
    latLng = location.coordinates.isNotEmpty
        ? LatLng(location.coordinates[0], location.coordinates[1])
        : null;
  }
  Completer<GoogleMapController> _controller = Completer();

  GoogleMapController? googleMapController;

  @override
  Widget build(BuildContext context) {
    // Provider.of<LocationServices>(context).locateMe();
    if (latLng != null) {
      return Stack(
        children: [
          GoogleMap(
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
              new Factory<OneSequenceGestureRecognizer>(
                () => new EagerGestureRecognizer(),
              ),
            ].toSet(),
            myLocationEnabled: true,
            zoomGesturesEnabled: true,
            zoomControlsEnabled: true,
            mapType: MapType.normal,
            myLocationButtonEnabled: true,
            initialCameraPosition: CameraPosition(target: latLng!, zoom: 14),
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
              googleMapController = controller;
            },
            markers: {
              Marker(
                markerId: MarkerId("businessLocation"),
                position: latLng!,
                infoWindow: const InfoWindow(title: 'Business Location'),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueRed),
              ),
            },
          ),
        ],
      );
    } else {
      return Center(
        child: MyText(
          "couldn't load location data",
          style: titleText,
        ),
      );
    }
  }
}
