import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/business/businesMainScreen.dart';
import 'package:kirb_mobile/Screens/business/businessLocationScreen.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/models/businessModel.dart';
import 'package:kirb_mobile/services/data/businessServices.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';
import 'package:provider/provider.dart';

class BusinessScreen extends StatefulWidget {
  final String id;
  const BusinessScreen({required this.id, Key? key}) : super(key: key);

  @override
  _BusinessScreenState createState() => _BusinessScreenState();
}

class _BusinessScreenState extends State<BusinessScreen> {
  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero).then((value) => {
          Provider.of<BusinessService>(context, listen: false)
              .getBusiness(context, widget.id)
        });
    return Material(
      color: Colors.white,
      child: Consumer<BusinessService>(
        builder: (_, business, child) {
          if (business.current != null)
            return DefaultTabController(
              length: 3,
              child: NestedScrollView(
                  headerSliverBuilder: (context, value) {
                    return [
                      SliverAppBar(
                        forceElevated: true,
                        elevation: 5,
                        expandedHeight: 250,
                        pinned: true,
                        title: Text(business.current!.name),
                        centerTitle: true,
                        shadowColor: customThemeData.accentColor,
                        flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.pin,
                          background: Image.asset(
                            'assets/images/trialImage.jpg',
                            color:
                                customThemeData.primaryColor.withOpacity(0.3),
                            colorBlendMode: BlendMode.lighten,
                            fit: BoxFit.fill,
                          ),
                        ),
                        leading: IconButton(
                          icon: Icon(
                            Icons.chevron_left_rounded,
                            color: Colors.black,
                          ),
                          onPressed: () => Navigator.pop(context),
                        ),
                        actions: [
                          IconButton(
                              onPressed: () {
                                business.onLike(context);
                              },
                              icon: Icon(
                                CustomIcons.heart,
                                color: business.favBusiness
                                        .contains(business.current)
                                    ? Colors.red
                                    : Colors.amber,
                              ))
                        ],
                        bottom: TabBar(
                          tabs: [
                            Tab(
                              iconMargin: EdgeInsets.zero,
                              icon: Icon(CustomIcons.info_square),
                              text: 'main',
                            ),
                            Tab(
                              iconMargin: EdgeInsets.zero,
                              icon: Icon(CustomIcons.location),
                              text: 'location',
                            ),
                            Tab(
                              iconMargin: EdgeInsets.zero,
                              icon: Icon(CustomIcons.calendar),
                              text: 'events',
                            ),
                          ],
                        ),
                      ),
                    ];
                  },
                  body: TabBarView(children: [
                    BusinessMainScreen(),
                    BusinessLocationScreen(
                        location: business.current!.location),
                    Center(
                      child: MyText('events', style: titleText),
                    ),
                  ])),
            );
          else
            return Center(
                child: MyText(
              "data not available",
              style: titleText,
            ));
        },
      ),
    );
  }
}
