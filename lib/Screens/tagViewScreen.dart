import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/constants/data.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class TagViewScreen extends StatefulWidget {
  int tagIndex;
  TagViewScreen({required this.tagIndex, Key? key}) : super(key: key);

  @override
  _TagViewScreenState createState() => _TagViewScreenState();
}

class _TagViewScreenState extends State<TagViewScreen> {
  List<PopupMenuEntry> menu = [];

  @override
  Widget build(BuildContext context) {
    menu.clear();
    tags.forEach((element) {
      menu.add(
        PopupMenuItem(
          value: tags.indexOf(element),
          child: Text(element),
        ),
      );
    });
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: MyText(
          "tag view",
          style: bigText,
        ),
        leading: IconButton(
          icon: Icon(
            Icons.chevron_left_rounded,
            size: 40,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  tags[this.widget.tagIndex],
                  style: midText,
                ),
                Row(
                  children: [
                    MyText(
                      'filter by',
                      style: discriptionText,
                    ),
                    PopupMenuButton(
                        icon: Icon(
                          CustomIcons.filter,
                          color: blueGrad.colors[1],
                        ),
                        onSelected: (value) {
                          setState(() {
                            widget.tagIndex = int.parse(value.toString());
                          });
                        },
                        itemBuilder: (BuildContext context) => menu),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
