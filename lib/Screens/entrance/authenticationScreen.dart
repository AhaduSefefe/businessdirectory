import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/entrance/loginScreen.dart';
import 'package:kirb_mobile/Screens/entrance/signupScreen.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class AuthenticationScreen extends StatelessWidget {
  static const String idScreen = "authentication";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (context, value) {
            return [
              SliverAppBar(
                title: MyText(
                  "kirb",
                  style: bigText,
                ),
                centerTitle: true,
                pinned: true,
                bottom: TabBar(
                  tabs: [
                    Tab(text: "Sign In"),
                    Tab(text: "Sign Up"),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            children: [
              LoginScreen(),
              SignupScreen(),
            ],
          ),
        ),
      ),
    );
  }
}
