import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/models/userModel.dart';
import 'package:kirb_mobile/services/data/userService.dart';
import 'package:kirb_mobile/services/validations.dart';
import 'package:kirb_mobile/widgets/myButton.dart';
import 'package:kirb_mobile/widgets/myContainer.dart';
import 'package:kirb_mobile/widgets/myTextField.dart';

class SignupScreen extends StatefulWidget {
  static const String idScreen = "signup";

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  var _formKey = GlobalKey<FormState>();
  bool obscure = true;
  Validator validator = Validator();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    MyTextField name, email, phone, password;
    return SingleChildScrollView(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: size.height * 0.05,
            ),
            MyContainer(
              width: size.width * 1,
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(height: 20),
                    name = MyTextField(
                      controller: TextEditingController(),
                      validator: validator.nameValidator,
                      icon: CustomIcons.user2,
                      label: "user name",
                      hintText: "User Name",
                      keyboardType: TextInputType.name,
                    ),
                    SizedBox(height: 20),
                    email = MyTextField(
                      controller: TextEditingController(),
                      validator: validator.emailValidator,
                      icon: CustomIcons.message,
                      label: "email",
                      hintText: "Email",
                      keyboardType: TextInputType.emailAddress,
                    ),
                    SizedBox(height: 20),
                    password = MyTextField(
                      validator: validator.passwordValidator,
                      controller: TextEditingController(),
                      icon: CustomIcons.lock,
                      label: "Password",
                      hintText: "Password",
                      obscureText: obscure,
                    ),
                    SizedBox(height: 20),
                    MyButton(
                      text: "sign up",
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          UserToSignup _user = UserToSignup(
                            name: name.controller!.text.trim(),
                            email: email.controller!.text.trim(),
                            password: password.controller!.text.trim(),
                          );
                          UserService _userService = UserService(context);
                          _userService.signUp(_user);
                        }
                      },
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            SizedBox(height: size.height * 0.01),
            Text(
              "By continuing, you agree to accept our \n Privacy Policy and Terms of Service.",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
              textAlign: TextAlign.center,
            )
          ]),
    );
  }
}
