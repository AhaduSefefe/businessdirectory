import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/models/userModel.dart';
import 'package:kirb_mobile/services/data/userService.dart';
import 'package:kirb_mobile/widgets/myButton.dart';
import 'package:kirb_mobile/widgets/myContainer.dart';
import 'package:kirb_mobile/widgets/myTextField.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static const String idScreen = "login";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // final Validator validator = Validator();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    MyTextField password, username;
    return Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.height * 0.1,
            ),
            MyContainer(
              width: size.width * 1,
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  SizedBox(height: 20),
                  username = MyTextField(
                    icon: CustomIcons.profile,
                    label: "username",
                    hintText: "username",
                    keyboardType: TextInputType.name,
                    controller: TextEditingController(),
                  ),
                  SizedBox(height: 20),
                  password = MyTextField(
                    icon: CustomIcons.lock,
                    label: "password",
                    hintText: "Password",
                    obscureText: true,
                    controller: TextEditingController(),
                  ),
                  SizedBox(height: 20),
                  MyButton(
                    text: "sign in",
                    onPressed: () {
                      UserToLogin _user = UserToLogin(
                        username: username.controller!.text.trim(),
                        password: password.controller!.text.trim(),
                      );
                      UserService userService = UserService(context);
                      userService.login(_user);
                    },
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              "By continuing, you agree to accept our \n Privacy Policy and Terms of Service.",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
