import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/main.dart';
import 'package:kirb_mobile/widgets/myButton.dart';
import 'package:kirb_mobile/widgets/myContainer.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.white,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MyText(
              "profile",
              style: bigText,
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CircleAvatar(
                  child: Image.asset("assets/images/profile.png"),
                  radius: 35,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Amanuel Tamiru",
                        style: titleText,
                        overflow: TextOverflow.fade,
                      ),
                      Text(
                        "Amantamiru1020@gmail.com",
                        style: discriptionText,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                MyButton(
                  text: "edit",
                  onPressed: () {},
                )
              ],
            ),
            SizedBox(height: 15),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  topContainer("Light", Icons.light_mode_outlined),
                  GestureDetector(
                      onTap: () {
                        MyApp.setLocale(context, Locale('am', 'ET'));
                      },
                      child: topContainer("English", Icons.language)),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: cardShadow),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    "accounts",
                    style: bigText,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  accountsRow(CustomIcons.paper, "my history"),
                  accountsRow(CustomIcons.profile, "edit profile"),
                  accountsRow(CustomIcons.password, "change password"),
                  accountsRow(CustomIcons.shield_done, "help and support"),
                  accountsRow(CustomIcons.logout, "log out"),
                ],
              ),
            ),
            aboutDevelopers(),
          ],
        ),
      ),
    );
  }

  Widget aboutDevelopers() {
    return MyContainer(
      margin: EdgeInsets.symmetric(vertical: 15),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      child: Column(
        children: [
          MyText(
            "about the developers",
            style: titleText,
          ),
          SizedBox(height: 15),
          Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi",
            style: discriptionText,
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }

  Widget accountsRow(IconData icon, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(
                icon,
                size: 30,
                color: redGrad.colors[0],
              ),
              SizedBox(width: 10),
              Text(
                text,
                style: midText,
              ),
            ],
          ),
          Icon(
            CustomIcons.arrowright_circle,
            size: 30,
            color: blueGrad.colors[0],
          )
        ],
      ),
    );
  }

  Widget topContainer(String text, IconData icon) {
    return MyContainer(
      width: 90,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 15,
      ),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            icon,
            size: 30,
            color: blueGrad.colors[1],
          ),
          SizedBox(height: 5),
          Text(
            text,
            style: midGreyText,
          ),
        ],
      ),
    );
  }
}
