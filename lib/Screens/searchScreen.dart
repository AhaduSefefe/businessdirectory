import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/business/businessScreen.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/listItemContainer.dart';
import 'package:kirb_mobile/widgets/myContainer.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class SearchScreen extends StatelessWidget {
  SearchScreen({Key? key}) : super(key: key);

  TextEditingController search = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(
      children: [
        SizedBox(
          height: 50,
        ),
        MyContainer(
          margin: EdgeInsets.symmetric(horizontal: 30),
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                CustomIcons.search,
                size: 25,
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                  child: TextField(
                controller: search,
                textAlignVertical: TextAlignVertical.bottom,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: "search"),
              )),
              SizedBox(
                width: 15,
              ),
              Icon(
                CustomIcons.filter,
                size: 25,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 50,
        ),
        Expanded(
          child: ListView.builder(
              itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  // onTap: () => Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => BusinessScreen()),
                  // ),
                  child: ListItemContainer(
                    title: 'SafeWay SuperMarket',
                    subtitle: 'Detail Goes Here',
                    prefix: CustomIcons.arrowright_circle,
                  ),
                );
              }),
        )
      ],
    ));
  }
}
