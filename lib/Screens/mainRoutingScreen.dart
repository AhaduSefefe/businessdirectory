import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/favoritesScreen.dart';
import 'package:kirb_mobile/Screens/homeScreen.dart';
import 'package:kirb_mobile/Screens/profileScreen.dart';
import 'package:kirb_mobile/Screens/searchScreen.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';

class MainRoutingScreen extends StatefulWidget {
  MainRoutingScreen({Key? key}) : super(key: key);

  @override
  _MainRoutingScreenState createState() => _MainRoutingScreenState();
}

class _MainRoutingScreenState extends State<MainRoutingScreen> {
  int _selectedIndex = 0;

  List<Widget> _children = [
    HomeScreen(),
    SearchScreen(),
    FavoriteScreen(),
    ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(height: size.height, child: _children[_selectedIndex]),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 40,
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(
                CustomIcons.home,
                size: 30,
              ),
              label: "home"),
          BottomNavigationBarItem(
              icon: Icon(CustomIcons.search), label: "search"),
          BottomNavigationBarItem(
            icon: Icon(
              CustomIcons.heart,
            ),
            label: "favorites",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              CustomIcons.profile,
            ),
            label: "me",
          ),
        ],
        currentIndex: _selectedIndex,
        unselectedItemColor: Colors.grey[500],
        selectedItemColor: customThemeData.accentColor,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
