import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/entrance/authenticationScreen.dart';
import 'package:kirb_mobile/Screens/tagViewScreen.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/constants/data.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class TagsList extends StatelessWidget {
  const TagsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            "tags",
            style: bigText,
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            height: 40,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, i) {
                return GestureDetector(
                    onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TagViewScreen(tagIndex: i)),
                        ),
                    child: tagContainer(i));
              },
              itemCount: tags.length,
            ),
          ),
          SizedBox(
            height: 15,
          )
        ],
      ),
    );
  }

  Widget tagContainer(int index) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        margin: EdgeInsets.only(right: 15),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), gradient: redGrad),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MyText(
              tags[index],
              style: midText,
            )
          ],
        ),
      ),
    );
  }
}
