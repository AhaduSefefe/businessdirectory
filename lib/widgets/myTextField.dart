import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/myContainer.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class MyTextField extends StatelessWidget {
  final String? hintText;
  final String label;
  final bool obscureText;
  final IconData? icon;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final FormFieldValidator? validator;
  const MyTextField({
    Key? key,
    required this.label,
    this.validator,
    this.icon,
    this.obscureText = false,
    this.hintText,
    required this.controller,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(this.label, style: titleText),
          SizedBox(
            height: size.height * 0.01,
          ),
          MyContainer(
            padding: EdgeInsets.only(left: 15, bottom: 0, top: 0),
            height: size.height * 0.05,
            child: Container(
              child: TextFormField(
                validator: validator,
                controller: controller,
                keyboardType: keyboardType,
                obscureText: obscureText,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                decoration: InputDecoration(
                  hintText: hintText,
                  icon: Icon(icon),
                  border: InputBorder.none,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
