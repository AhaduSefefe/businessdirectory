import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';

class MyContainer extends StatelessWidget {
  const MyContainer(
      {required this.child,
      this.margin,
      this.padding: const EdgeInsets.all(15),
      this.height,
      this.width,
      Key? key})
      : super(key: key);
  final Widget child;
  final double? height;
  final double? width;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  @override
  Widget build(BuildContext context) {
    return Container(
        clipBehavior: Clip.hardEdge,
        height: height,
        width: width,
        padding: padding,
        margin: margin,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            boxShadow: cardShadow,
            color: Colors.white),
        child: child);
  }
}
