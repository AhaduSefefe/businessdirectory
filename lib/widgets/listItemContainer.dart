import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

import 'myContainer.dart';

class ListItemContainer extends StatelessWidget {
  final String title;
  final String? subtitle;
  final IconData? prefix;
  const ListItemContainer(
      {required this.title, this.subtitle, this.prefix, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyContainer(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
      padding: EdgeInsets.only(right: 10),
      height: 80,
      width: 315,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            'assets/images/trialImage.jpg',
            height: 80,
            width: 100,
            fit: BoxFit.cover,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      this.title,
                      style: titleText,
                    ),
                    if (this.subtitle != null)
                      MyText(
                        this.subtitle!,
                        style: discriptionText,
                      )
                  ],
                ),
                if (this.prefix != null)
                  Icon(
                    this.prefix,
                    size: 30,
                    color: blueGrad.colors[1],
                  )
              ],
            ),
          )
        ],
      ),
    );
  }
}
