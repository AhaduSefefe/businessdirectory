import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';

class MyButton extends StatelessWidget {
  MyButton({required this.text, required this.onPressed, Key? key})
      : super(key: key);
  final String text;
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 80,
        height: 40,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(99), gradient: blueGrad),
        child: Center(
            child: MyText(
          this.text,
          style: midText,
        )),
      ),
    );
  }
}
