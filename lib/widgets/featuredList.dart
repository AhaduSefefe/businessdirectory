import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/business/businessScreen.dart';
import 'package:kirb_mobile/constants/customIcons.dart';
import 'package:kirb_mobile/constants/cutomThemeData.dart';
import 'package:kirb_mobile/models/businessModel.dart';
import 'package:kirb_mobile/services/data/businessServices.dart';
import 'package:kirb_mobile/widgets/myContainer.dart';
import 'package:kirb_mobile/widgets/text/myText.dart';
import 'package:provider/provider.dart';

class FeaturedList extends StatelessWidget {
  const FeaturedList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero).then((value) => {
          Provider.of<BusinessService>(context, listen: false)
              .getAllBusiness(context)
        });
    return Expanded(
      child: Consumer<BusinessService>(
        builder: (_, business, child) {
          if (business.businessAll.isNotEmpty) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  MyText(
                    'featured',
                    style: bigText,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Expanded(
                    child: Container(
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: AlwaysScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, i) {
                          return setFeatured(context, business.businessAll[i]);
                        },
                        itemCount: business.businessAll.length,
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else
            return Center(
                child: MyText(
              "data not available",
              style: titleText,
            ));
        },
      ),
    );
  }

  Widget setFeatured(BuildContext context, Business business) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BusinessScreen(
                  id: business.id,
                )),
      ),
      child: MyContainer(
        margin: EdgeInsets.only(
          bottom: 30,
        ),
        padding: null,
        height: 400,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              'assets/images/trialImage.jpg',
              height: 300,
              fit: BoxFit.cover,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              color: Colors.red[50],
              height: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    business.name,
                    style: titleText,
                  ),
                  Text(
                    business.description,
                    style: discriptionText,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        CustomIcons.heart,
                        size: 15,
                      ),
                      MyText(
                        business.likeCount.toString(),
                        style: discriptionText,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        CustomIcons.location,
                        size: 15,
                      ),
                      MyText(
                        'Megenagna',
                        style: discriptionText,
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
