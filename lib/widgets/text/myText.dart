import 'package:flutter/material.dart';
import 'package:kirb_mobile/services/localization/translate.dart';

class MyText extends StatelessWidget {
  const MyText(this.text, {this.style, Key? key}) : super(key: key);
  final String text;
  final TextStyle? style;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        translate(context, text),
        style: this.style,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
