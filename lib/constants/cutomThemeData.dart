import 'dart:ui';

import 'package:flutter/material.dart';

ThemeData customThemeData = ThemeData(
    fontFamily: 'Poppins',
    primaryColor: Colors.white,
    accentColor: Colors.redAccent,
    iconTheme: IconThemeData(size: 20, color: Colors.grey),
    primaryIconTheme: IconThemeData(
      size: 20,
      color: Colors.red,
    ));

Gradient redGrad = LinearGradient(colors: [
  Color.fromRGBO(238, 164, 206, .5),
  Color.fromRGBO(197, 139, 242, .5),
]);
Gradient blueGrad = LinearGradient(colors: [
  Color.fromRGBO(157, 206, 255, 1),
  Color.fromRGBO(146, 163, 253, 1)
]);

List<BoxShadow> cardShadow = [
  BoxShadow(
      color: Color.fromRGBO(29, 22, 23, 0.07),
      blurRadius: 20,
      offset: Offset(0, 20))
];
List<BoxShadow> inputShadow = [
  BoxShadow(
      color: Color.fromRGBO(29, 22, 23, 0.07),
      blurRadius: 40,
      offset: Offset(0, 10))
];

TextStyle bigText = TextStyle(fontWeight: FontWeight.w700, fontSize: 20);
TextStyle midText = TextStyle(fontWeight: FontWeight.w400, fontSize: 16);
TextStyle midGreyText = TextStyle(
  fontWeight: FontWeight.w700,
  fontSize: 16,
  color: Color.fromRGBO(123, 111, 114, 1),
);
TextStyle titleText = TextStyle(fontWeight: FontWeight.w700, fontSize: 16);
TextStyle discriptionText = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 14,
    color: Color.fromRGBO(123, 111, 114, 1));
