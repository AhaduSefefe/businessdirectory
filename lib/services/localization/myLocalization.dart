import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyLocalization {
  final Locale locale;
  MyLocalization({required this.locale});

  static MyLocalization? of(BuildContext context) {
    return Localizations.of<MyLocalization>(context, MyLocalization);
  }

  Map<String, String> localizedValues = {};

  Future load() async {
    String jsonValues = await rootBundle
        .loadString("lib/services/lang/${locale.languageCode}.json");

    Map<String, dynamic> mappedJson = jsonDecode(jsonValues);

    localizedValues =
        mappedJson.map((key, value) => MapEntry(key, value.toString()));
  }

  static const LocalizationsDelegate<MyLocalization> delegate =
      DelegateMyLocalization();

  String getTranslatedValue(String key) {
    if (localizedValues.containsKey(key))
      return localizedValues[key] != null ? localizedValues[key]! : key;
    else
      return key;
  }
}

class DelegateMyLocalization extends LocalizationsDelegate<MyLocalization> {
  const DelegateMyLocalization();
  @override
  bool isSupported(Locale locale) {
    return ['en', 'am'].contains(locale.languageCode);
  }

  @override
  Future<MyLocalization> load(Locale locale) async {
    MyLocalization localization = MyLocalization(locale: locale);
    await localization.load();
    return localization;
  }

  @override
  bool shouldReload(DelegateMyLocalization old) => false;
}
