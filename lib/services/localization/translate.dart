import 'package:flutter/cupertino.dart';
import 'package:kirb_mobile/services/localization/myLocalization.dart';

String translate(BuildContext context, String key) {
  return MyLocalization.of(context)!.getTranslatedValue(key);
}
