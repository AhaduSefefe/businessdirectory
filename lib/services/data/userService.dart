import 'dart:convert' as convert;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kirb_mobile/Screens/entrance/authenticationScreen.dart';
import 'package:kirb_mobile/Screens/mainRoutingScreen.dart';
import 'package:kirb_mobile/models/userModel.dart';
import 'package:kirb_mobile/services/data/crud.dart';
import 'package:kirb_mobile/services/handler/responseHandler.dart';
import 'package:kirb_mobile/services/handler/sharedPrefrenceHandler.dart';
import 'package:kirb_mobile/services/waitingDialogue.dart';

class UserService {
  var response;
  String? id;
  ResponseHandler? responseHandler;
  BuildContext? context;
  Data? _data;
  UserService(BuildContext context) {
    this.context = context;
    if (SharedPrefrenceHandler.keyExists("_id"))
      this.id = SharedPrefrenceHandler.get("id");
    this.responseHandler = ResponseHandler();
  }

  login(UserToLogin _user) async {
    WaitingDialogue.showDialogue(
      this.context!,
      "Signing In",
    );

    _data = Data("auth/login", withToken: false);

    User user;
    await _data!.setData(_user.toJson(), context!).then((value) => {
          this.response = responseHandler!.checkResponse(value),
          if (this.response != null)
            {
              SharedPrefrenceHandler.set(
                  "token", convert.jsonDecode(this.response)["token"]),
              print(SharedPrefrenceHandler.get("token")),
              Navigator.pushAndRemoveUntil(
                  context!,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => MainRoutingScreen()),
                  (route) => false)
            }
          else
            Navigator.pop(context!)
        });
  }

  logout() async {
    SharedPrefrenceHandler.clear();
    Navigator.pushAndRemoveUntil(
        context!,
        MaterialPageRoute<void>(
            builder: (BuildContext context) => AuthenticationScreen()),
        (route) => false);
  }

  signUp(UserToSignup _user) async {
    await WaitingDialogue.showDialogue(
      this.context!,
      "Signing Up",
    );

    _data = Data("auth/", withToken: false);

    await _data!.setData(_user.toJson(), context!).then((value) => {
          this.response = responseHandler!.checkResponse(value),
          print("##############################" + this.response.toString()),
          if (this.response != null)
            {
              SharedPrefrenceHandler.set(
                  "token", convert.jsonDecode(this.response)["token"]),
              Navigator.pushAndRemoveUntil(
                  context!,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) =>
                          AuthenticationScreen()),
                  (route) => false)
            }
          else
            Navigator.pop(context!)
        });
  }

  update(User _user) async {
    WaitingDialogue.showDialogue(
      this.context!,
      "Updating Profile",
    );

    _data = Data("updateprofile");

    _data!.updateData(_user.toJson(), context!).then((value) => {
          this.response = responseHandler!.checkResponse(value),
          if (this.response != null)
            {SharedPrefrenceHandler.setUserProfile(_user)},
        });
    Navigator.pop(context!);
  }

  getProfile() async {
    return SharedPrefrenceHandler.getUserProfile();
  }
}
