import 'dart:async';

import 'package:flutter/material.dart';

import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:kirb_mobile/services/handler/errorHandler.dart';
import 'package:kirb_mobile/services/handler/sharedPrefrenceHandler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../constants/data.dart' as globals;

class Data {
  var url;
  var path;
  ErrorHandler errorHandler = ErrorHandler();
  Map<String, String>? headers;
  Data(String path, {bool withToken = true}) {
    this.url = globals.url;
    this.path = path;
    headers = <String, String>{
      'Content-Type': 'application/json',
    };
    if (withToken) {
      setToken();
    }
  }

  setToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? tok = prefs.getString("token");
    headers!.putIfAbsent('Authorization', () => tok!);
  }

  getData() async {
    print(Uri.http(this.url, this.path).toString());

    return http
        .get(Uri.http(this.url, this.path), headers: headers)
        // ignore: return_of_invalid_type_from_catch_error
        .catchError((onError) => {
              errorHandler.showErrors(onError.toString()),
            });
  }

  setData(Object data, BuildContext context) async {
    print(convert.jsonEncode(data));
    print(Uri.http(this.url, this.path).toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tok = prefs.get("token");
    return http
        .post(
          Uri.http(this.url, this.path),
          headers: headers,
          body: convert.jsonEncode(data),
        )
        // ignore: return_of_invalid_type_from_catch_error
        .catchError((onError) => {
              errorHandler.showErrors(onError.toString()),
              Navigator.pop(context),
            });
  }

  createData(Object data, BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tok = prefs.get("token");
    print(Uri.http(this.url, this.path));
    return http
        .post(
          Uri.http(this.url, this.path),
          headers: headers,
          body: convert.jsonEncode(data),
        )
        // ignore: return_of_invalid_type_from_catch_error
        .catchError((onError) => {
              errorHandler.showErrors(onError.toString()),
              Navigator.pop(context),
            });
  }

  updateData(Object data, BuildContext context) async {
    var tok = SharedPrefrenceHandler.get("token");
    print(Uri.http(this.url, this.path));
    return http
        .put(
          Uri.http(this.url, this.path),
          headers: headers,
          body: convert.jsonEncode(data),
        )
        // ignore: return_of_invalid_type_from_catch_error
        .catchError((onError) => {
              errorHandler.showErrors(onError.toString()),
              Navigator.pop(context),
            });
  }

  deleteData(var id) async {
    final http.Response response = await http.delete(
      Uri.http(this.url, this.path + "/" + id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      print("deleted data");
      // return convert.jsonDecode(response.body);
    } else {
      throw Exception("Can't delete data. Error of type " +
          response.statusCode.toString() +
          " happened");
    }
  }
}
