import 'dart:convert' as convert;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kirb_mobile/models/businessModel.dart';
import 'package:kirb_mobile/services/data/crud.dart';
import 'package:kirb_mobile/services/handler/responseHandler.dart';
import 'package:kirb_mobile/services/handler/sharedPrefrenceHandler.dart';
import 'package:kirb_mobile/services/waitingDialogue.dart';

class BusinessService extends ChangeNotifier {
  var response;
  ResponseHandler responseHandler = ResponseHandler();
  Data? _data;
  List<Business> businessAll = [];
  List<Business> favBusiness = [];
  Business? current;

  getAllBusiness(BuildContext context) async {
    await WaitingDialogue.showDialogue(
      context,
      "Retriving Businesses",
    );
    businessAll.clear();

    _data = Data("/business");
    await _data!.getData().then((value) => {
          this.response = responseHandler.checkResponse(value),
          if (response != null)
            {
              convert.jsonDecode(response.toString()).forEach((element) => {
                    businessAll.add(Business.fromJson((element))),
                  }),
            }
        });
    notifyListeners();
    Navigator.pop(context);
  }

  getBusiness(BuildContext context, String id) async {
    await WaitingDialogue.showDialogue(
      context,
      "Retriving Business ",
    );

    _data = Data("/business/$id");
    var res;
    await _data!.getData().then((value) => {
          res = responseHandler.checkResponse(value),
          print(res),
          if (res != null)
            {
              current = Business.fromJson(convert.jsonDecode(res)),
            }
        });

    notifyListeners();
    Navigator.pop(context);
  }

  onLike(BuildContext context) async {
    _data = Data("/business/like");
    var res;
    await _data!.setData({"business": current!.id}, context).then((value) => {
          res = responseHandler.checkResponse(value),
          print(res),
          if (res != null) {favBusiness.add(current!)}
        });
  }
}
