import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:kirb_mobile/services/toastService.dart';

class ResponseHandler {
  Toast toast = Toast();
  checkResponse(http.Response response, {bool showToast = true}) {
    if (response.statusCode == 200) {
      if (showToast) toast.successNotification("Successfull");
      return response.body;
    } else {
      toast.errorNotification("Failed :" +
          (convert.jsonDecode(response.body.toString())["msg"] ??=
                  "Data not recieved")
              .toString()
              .toUpperCase());
      print("Error: " + response.body);
      return null;
    }
  }
}
