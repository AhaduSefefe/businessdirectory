import 'package:kirb_mobile/services/toastService.dart';

class ErrorHandler {
  Toast toast = Toast();
  showErrors(String error) {
    print(error);
    toast.errorNotification("error");
  }
}
