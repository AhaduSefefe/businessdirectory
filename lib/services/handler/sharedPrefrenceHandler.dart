import 'dart:convert' as convert;

import 'package:kirb_mobile/models/userModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefrenceHandler {
  static SharedPreferences? preferences;

  static Future init() async =>
      preferences = await SharedPreferences.getInstance();

  static get(String key) {
    return preferences!.getString(key);
  }

  static set(String key, dynamic data) {
    preferences!.setString(key, data);
  }

  static remove(String key) {
    preferences!.remove(key);
  }

  static setUserProfile(User user) {
    preferences!.setString("user", convert.jsonEncode(user.toJson()));
    preferences!.setString("_id", user.id);
  }

  static User getUserProfile() {
    return User.fromJson(convert.jsonDecode(preferences!.getString("user")!));
  }

  // static setExpert(ExpertToList expert) {
  //   preferences.setString("expert", convert.jsonEncode(expert.toJson()));
  // }

  // static ExpertToList getExpert() {
  //   print(ExpertToList.fromJson(convert.jsonDecode(preferences.get("expert")))
  //       .id);
  //   return ExpertToList.fromJson(convert.jsonDecode(preferences.get("expert")));
  // }

  // static List<Expense> getExpenses() {
  //   List<Expense> expenses = [];
  //   convert.jsonDecode(preferences.get("expenses")).forEach((value) => {
  //         expenses.add(Expense.fromJson(value)),
  //       });
  //   return expenses;
  // }

  // static setExpenses(List<Expense> expenses) {
  //   List<String> data = [];
  //   expenses.forEach((element) {
  //     data.add(convert.jsonEncode(element.toJson()));
  //   });
  //   preferences.setString("expenses", data.toString());
  // }

  // static setSavings(List<Saving> savings) {
  //   List<String> data = [];
  //   savings.forEach((element) {
  //     data.add(convert.jsonEncode(element.toJson()));
  //   });
  //   preferences.setString("savings", data.toString());
  // }

  // static setIncomes(List<Income> incomes) {
  //   List<String> data = [];
  //   incomes.forEach((element) {
  //     data.add(convert.jsonEncode(element.toJson()));
  //   });
  //   preferences.setString("incomes", data.toString());
  // }

  // static List<Saving> getSavings() {
  //   List<Saving> savings = [];
  //   convert.jsonDecode(preferences.get("savings")).forEach((value) => {
  //         savings.add(Saving.fromJson(value)),
  //       });
  //   return savings;
  // }

  // static List<Income> getIncomes() {
  //   List<Income> incomes = [];
  //   convert.jsonDecode(preferences.get("incomes")).forEach((value) => {
  //         incomes.add(Income.fromJson(value)),
  //       });
  //   return incomes;
  // }

  // static setWallet(Wallet wallet) {
  //   preferences.setString("wallet", wallet.toJson().toString());
  // }

  // static Wallet getWallet() {
  //   return Wallet.fromJson(convert.jsonDecode(preferences.get("wallet")));
  // }

  // static setGoal(String type, Goal goal) {
  //   preferences.setString(type, convert.jsonEncode(goal.toJson()));
  // }

  // static Goal getGoal(String type) {
  //   print("$type" + preferences.get(type));
  //   return Goal.fromJson(convert.jsonDecode(preferences.get(type)));
  // }

  // static AvailableTime getMeetingTime() {
  //   return AvailableTime.fromJson(
  //       convert.jsonDecode(preferences.get("meetingTime")));
  // }

  // static setMeetingTime(AvailableTime time) {
  //   preferences.setString("meetingTime", convert.jsonEncode(time.toJson()));
  // }

  static clear() {
    preferences!.clear();
  }

  static bool keyExists(String key) {
    if (preferences!.containsKey(key)) return true;
    return false;
  }
}
