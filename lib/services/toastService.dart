import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Toast {
  void errorNotification(String message) {
    Fluttertoast.showToast(
      timeInSecForIosWeb: 5,
      msg: message,
      backgroundColor: Colors.red,
      textColor: Colors.white,
    );
  }

  void successNotification(String message) {
    Fluttertoast.showToast(
      timeInSecForIosWeb: 5,
      msg: message,
      backgroundColor: Colors.green,
      textColor: Colors.white,
    );
  }
}
