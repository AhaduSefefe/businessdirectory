import 'package:flutter/material.dart';
import 'package:kirb_mobile/widgets/progressDialog.dart';

class WaitingDialogue {
  static showDialogue(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ProgressDialog(
            message: message,
          );
        });
  }
}
